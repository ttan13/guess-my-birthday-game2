from random import randint

# ask user name w prompt "Hi! What is your name?"

# tries to guess birth month and year w/ a prompt formatted like
# Guess <guess number> : <name> were you born in <m> / <yyyy> ?
# then prompt with yes or no?

# if computer guesses correctly when user replies yes, it prints the message I knew
# it! and stops guessing

# If the computer guesses incorrectly because you respond no, it prints the 
# message Drat! Lemme try again! if it's only guessed 1, 2, 3, or 4 times. 
# Otherwise, it prints the message I have other things to do. Good bye.
# It should only guess years between 1924 and 2004 including those years.

name = input("Hi! What is your name? ")

for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess",  guess_number, " : ",  name, "were you born on",  
    month_number, " / ",  year_number, " ?")

    response = input("yes or no?\n")

    if response == ("yes"):
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")






